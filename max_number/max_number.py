from datetime import datetime

import faust

from faust.cli import argument, option

app = faust.App('max-number', broker='kafka://kafka:9092')


class Number(faust.Record):
    number: int
    created: datetime = datetime.now()


topic = app.topic('max-number', value_type=Number, partitions=1)
reset_topic = app.topic('reset-number', partitions=1)


table = app.Table('max-number', default=int, partitions=1)


@app.agent(topic)
async def process_max_number(stream):
    async for number in stream:
        if number.number > table['max']:
            print(f'setting new max number to {number} (was {table["max"]}), created at {number.created}')
            table['max'] = number.number


@app.agent(reset_topic)
async def reset_max_number(stream):
    print('resettings max number to 0')
    async for _ in stream:
        table['max'] = 0


@app.command(
    argument('number')
)
async def send_number(self, number):
    await process_max_number.cast(Number(number=int(number)))


@app.command()
async def reset_number(self):
    await reset_max_number.cast()
