FROM python:3.8.5-buster

RUN pip install faust==1.10.3
RUN mkdir /app
